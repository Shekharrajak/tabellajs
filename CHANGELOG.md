# Changelog

##v 0.3.5 - March 2015, 30th
- Fix fixed-header behavior for really long tables
- Fix .t-element class CSS, update to 100% width

##v 0.3.4 - March 2015, 30th
- Fix fixed-header behavior on resize event for mobile browsers
- Fix fixed-header beackground flickering

##v 0.3.2 - March 2015, 27th
- Add fixed-header option, enabled by default

##v 0.3.1 - March 2015, 26th
- Fix arrows' click during animation
- Fix scroll & swipe conflict
- Fix second to last element on swipe right behavior

##v 0.3.0 - March 2015, 25th
- Fix empty rows and empty row description behavior
- Fix - don't add currency if the cell value is not a number
- Update namespace to meet the new multipurpose nature of the tables


##v 0.2.0 - March 2015, 20th
- Add different header content capability 
